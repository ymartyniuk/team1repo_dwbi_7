IF NOT EXISTS (SELECT TOP 1 * FROM [dim].[CALENDAR])
BEGIN
DECLARE @MIN_DATE DATE = '01012008' ,
                 @NUMBER_OF_YEARS INT = 5

SET DATEFIRST 7;
SET DATEFORMAT mdy;
SET LANGUAGE US_ENGLISH;

DECLARE @CUTOFF_DATE DATE = DATEADD(YEAR, @NUMBER_OF_YEARS, @MIN_DATE);
 
INSERT INTO [dim].[CALENDAR]([DATE_ID]) 
SELECT d
FROM
(
  SELECT d = DATEADD(DAY, rn - 1, @MIN_DATE)
  FROM 
  (
    SELECT TOP (DATEDIFF(DAY, @MIN_DATE, @CUTOFF_DATE)) 
      rn = ROW_NUMBER() OVER (ORDER BY s1.[object_id])
    FROM sys.all_objects AS s1
    CROSS JOIN sys.all_objects AS s2
    -- on my system this would support > 5 million days
    ORDER BY s1.[object_id]
  ) AS x
) AS y
END
GO