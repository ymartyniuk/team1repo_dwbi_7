USE DW_DS2
GO

-------------------------------------------------------------------------------------------------
-----------------------------  DROP FOREIGN KEYS ------------------------------------------------
-------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dim.FK_geo_geography')
  )
ALTER TABLE [dim].[GEOGRAPHY] DROP CONSTRAINT [FK_geo_geography]
GO
-------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------
IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'fact.FK_orderlines_product')
  )
ALTER TABLE [fact].[ORDERLINES] DROP CONSTRAINT [FK_orderlines_product]
GO
-------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'fact.FK_orderlines_calendar')
  )
ALTER TABLE [fact].[ORDERLINES] DROP CONSTRAINT [FK_orderlines_calendar]
GO
-------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'fact.FK_orderlines_agegroups')
  )
ALTER TABLE [fact].[ORDERLINES] DROP CONSTRAINT [FK_orderlines_agegroups]
GO
-------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'fact.FK_orderlines_customer')
  )
ALTER TABLE [fact].[ORDERLINES] DROP CONSTRAINT [FK_orderlines_customer]
GO
-------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'fact.FK_orderlines_gender')
  )
ALTER TABLE [fact].[ORDERLINES] DROP CONSTRAINT [FK_orderlines_gender]
GO
-------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'fact.FK_orderlines_geography')
  )
ALTER TABLE [fact].[ORDERLINES] DROP CONSTRAINT [FK_orderlines_geography]
GO
-------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'fact.FK_orderlines_income')
  )
ALTER TABLE [fact].[ORDERLINES] DROP CONSTRAINT [FK_orderlines_income]
GO
-------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------
-----------------------------  DROP TABLES ------------------------------------------------------
-------------------------------------------------------------------------------------------------
IF OBJECT_ID('dim.AGEGROUPS', 'U') IS NOT NULL 
DROP TABLE [dim].[AGEGROUPS]
GO

IF OBJECT_ID('dim.GEOGRAPHY', 'U') IS NOT NULL 
DROP TABLE [dim].[GEOGRAPHY]
GO

IF OBJECT_ID('dim.GENDER', 'U') IS NOT NULL 
DROP TABLE [dim].[GENDER]
GO

IF OBJECT_ID('dim.INCOME', 'U') IS NOT NULL 
DROP TABLE [dim].[INCOME]
GO

IF OBJECT_ID('dim.PRODUCT', 'U') IS NOT NULL 
DROP TABLE [dim].[PRODUCT]
GO

IF OBJECT_ID('dim.CUSTOMER', 'U') IS NOT NULL 
DROP TABLE [dim].[CUSTOMER]
GO

IF OBJECT_ID('dim.CALENDAR', 'U') IS NOT NULL 
DROP TABLE [dim].[CALENDAR]
GO

IF OBJECT_ID('fact.ORDERLINES', 'U') IS NOT NULL 
DROP TABLE [fact].[ORDERLINES]
GO
-------------------------------------------------------------------------------------------------
------------------------------ TABLES CREATION --------------------------------------------------
-------------------------------------------------------------------------------------------------
------------------------------ DIMENSION TABLES
CREATE TABLE [dim].[AGEGROUPS](
    [AGEGROUP_ID] [INT] IDENTITY(1,1) NOT NULL,
    [AGEGROUP] [NVARCHAR](10) NOT NULL,
    [MIN_AGE] [TINYINT] NOT NULL,
    [MAX_AGE] [TINYINT] NOT NULL,
	[CREATE_DATE] [DATETIME] NOT NULL,
	[UPDATE_DATE] [DATETIME] NOT NULL,
	CONSTRAINT [PK_agegroups] PRIMARY KEY CLUSTERED ([AGEGROUP_ID] ASC)
)
GO
-------------------------------------------------------------------------------------------------
CREATE TABLE [dim].[GEOGRAPHY](
    [GEOGRAPHY_ID] [INT] IDENTITY (1,1) NOT NULL,
	[GEOGRAPHY_NAME] [NVARCHAR](50) NOT NULL,
	[GEO_PARENT_ID] [INT] NULL,
	[CREATE_DATE] [DATETIME] NOT NULL,
	[UPDATE_DATE] [DATETIME] NOT NULL,
	CONSTRAINT [PK_geography] PRIMARY KEY CLUSTERED ([GEOGRAPHY_ID] ASC),
	CONSTRAINT [AK_geography] UNIQUE ([GEOGRAPHY_NAME],[GEO_PARENT_ID])
)
GO
-------------------------------------------------------------------------------------------------
CREATE TABLE [dim].[GENDER](
    [GENDER_ID] [INT] IDENTITY(1,1) NOT NULL,
	[GENDER] [NCHAR](1) NOT NULL,
	[CREATE_DATE] [DATETIME] NOT NULL,
	[UPDATE_DATE] [DATETIME] NOT NULL,
	CONSTRAINT [PK_gender] PRIMARY KEY CLUSTERED ([GENDER_ID] ASC)
)
GO
-------------------------------------------------------------------------------------------------
CREATE TABLE [dim].[INCOME](
    [INCOME_ID] [INT] IDENTITY(1,1) NOT NULL,
	[INCOME] [NVARCHAR](20) NOT NULL,
	[MIN_INCOME] [INT] NOT NULL,
	[MAX_INCOME] [INT] NOT NULL,
	[CREATE_DATE] [DATETIME] NOT NULL,
	[UPDATE_DATE] [DATETIME] NOT NULL,
	CONSTRAINT [PK_income] PRIMARY KEY CLUSTERED ([INCOME_ID] ASC)
)
GO
-------------------------------------------------------------------------------------------------
CREATE TABLE [dim].[PRODUCT](
    [PRODUCT_ID] [INT] IDENTITY(1,1) NOT NULL,
	[SOURCE_PRODUCT_ID] [INT] UNIQUE NOT NULL,
	[TITLE] [NVARCHAR](50) NOT NULL,
	[CATEGORY] [NVARCHAR](50) NOT NULL,
	[ACTOR] [NVARCHAR](50) NOT NULL,
	[PRICE] [MONEY] NOT NULL,
	[CREATE_DATE] [DATETIME] NOT NULL,
	[UPDATE_DATE] [DATETIME] NOT NULL,
	CONSTRAINT [PK_product] PRIMARY KEY CLUSTERED ([PRODUCT_ID] ASC)
)
GO
-------------------------------------------------------------------------------------------------
CREATE TABLE [dim].[CUSTOMER](
    [CUSTOMER_ID] [INT] IDENTITY(1,1) NOT NULL,
	[SOURCE_CUSTOMER_ID] [INT] UNIQUE NOT NULL,
	[FIRSTNAME] [NVARCHAR](50) NOT NULL,
	[LASTNAME] [NVARCHAR](50) NOT NULL,
	[EMAIL] [NVARCHAR](50) NULL,
	[PHONE] [NVARCHAR](50) NULL,
	[AGE] [TINYINT] NULL,
	[CITY] [NVARCHAR](50) NOT NULL,
	[STATE] [NVARCHAR](50) NULL,
	[COUNTRY] [NVARCHAR](50) NOT NULL,
	[REGION] [TINYINT] NOT NULL,
	[CREATE_DATE] [DATETIME] NOT NULL,
	[UPDATE_DATE] [DATETIME] NOT NULL,
	CONSTRAINT [PK_customer] PRIMARY KEY CLUSTERED ([CUSTOMER_ID] ASC)
)
GO
-------------------------------------------------------------------------------------------------
CREATE TABLE [dim].[CALENDAR](
    [DATE_ID]        DATE NOT NULL,
    [DAY]            AS DATEPART(DAY,      [DATE_ID]),
    [MONTH]          AS DATEPART(MONTH,    [DATE_ID]),
    [FIRST_OF_MONTH] AS CONVERT(DATE, DATEADD(MONTH, DATEDIFF(MONTH, 0, [DATE_ID]), 0)),
    [MONTH_NAME]     AS DATENAME(MONTH,    [DATE_ID]),
    [WEEK]           AS DATEPART(WEEK,     [DATE_ID]),
	[WEEK_OF_MONTH]  AS DATEPART(WEEK, [DATE_ID])-DATEPART(WEEK, DATEADD(MM, DATEDIFF(MM,0,[DATE_ID]), 0))+ 1,
    [ISO_WEEK]       AS DATEPART(ISO_WEEK, [DATE_ID]),
    [DAY_OF_WEEK]    AS DATEPART(WEEKDAY,  [DATE_ID]),
    [QUATER]         AS DATEPART(QUARTER,  [DATE_ID]),
    [YEAR]           AS DATEPART(YEAR,     [DATE_ID]),
    [FIRST_OF_YEAR]  AS CONVERT(DATE, DATEADD(YEAR,  DATEDIFF(YEAR,  0, [DATE_ID]), 0)),
    [STYLE_101]      AS CONVERT(CHAR(10),  [DATE_ID], 101),
	[STYLE_102]      AS CONVERT(CHAR(8),   [DATE_ID], 112),
	[CREATE_DATE] [DATETIME] NOT NULL,
	[UPDATE_DATE] [DATETIME] NOT NULL,
    CONSTRAINT [PK_calendar] PRIMARY KEY CLUSTERED ([DATE_ID] ASC)
)
GO
-------------------------------------------------------------------------------------------------
------------------------------ FACT TABLES
-------------------------------------------------------------------------------------------------
CREATE TABLE [fact].[ORDERLINES](
    [ORDERLINES_ID] [INT] IDENTITY (1,1) NOT NULL,
	[SOURCE_ORDERLINES_ID] [INT] NOT NULL,
	[SOURCE_ORDERS_ID] [INT] NOT NULL,
	[PRODUCT_ID] [INT] NOT NULL,
    [DATE_ID] [DATE] NOT NULL,
	[CUSTOMER_ID] [INT] NOT NULL,
	[GENDER_ID] [INT] NOT NULL,
	[GEOGRAPHY_ID] [INT] NOT NULL,
	[INCOME_ID] [INT] NOT NULL,
	[AGEGROUP_ID] [INT] NOT NULL,
	[PRODUCT_PRICE] [MONEY] NOT NULL,
	[QUANTITY] [SMALLINT] NOT NULL,
	[CREATE_DATE] [DATETIME] NOT NULL,
	[UPDATE_DATE] [DATETIME] NOT NULL,
	CONSTRAINT [PK_orderlines] PRIMARY KEY CLUSTERED ([ORDERLINES_ID] ASC)
)
GO
-------------------------------------------------------------------------------------------------
------------------------------ FOREIGN KEYS -----------------------------------------------------
-------------------------------------------------------------------------------------------------
------------------------ GEOGRAPHY - GEOGRAFY
ALTER TABLE [dim].[GEOGRAPHY]  WITH CHECK ADD  CONSTRAINT [FK_geo_geography] FOREIGN KEY([GEO_PARENT_ID])
REFERENCES  [dim].[GEOGRAPHY] ([GEOGRAPHY_ID])
GO
ALTER TABLE [dim].[GEOGRAPHY] CHECK CONSTRAINT [FK_geo_geography]
GO
-------------------------------------------------------------------------------------------------
------------------------ ORDERLINE - PRODUCT
ALTER TABLE [fact].[ORDERLINES]  WITH CHECK ADD  CONSTRAINT [FK_orderlines_product] FOREIGN KEY([PRODUCT_ID])
REFERENCES  [dim].[PRODUCT] ([PRODUCT_ID])
GO
ALTER TABLE [fact].[ORDERLINES] CHECK CONSTRAINT [FK_orderlines_product]
GO
-------------------------------------------------------------------------------------------------
------------------------ ORDERLINE - CALENDAR
ALTER TABLE [fact].[ORDERLINES]  WITH CHECK ADD  CONSTRAINT [FK_orderlines_calendar] FOREIGN KEY([DATE_ID])
REFERENCES  [dim].[CALENDAR] ([DATE_ID])
GO
ALTER TABLE [fact].[ORDERLINES] CHECK CONSTRAINT [FK_orderlines_calendar]
GO
------------------------ ORDERLINE - AGEGROUPS
ALTER TABLE [fact].[ORDERLINES]  WITH CHECK ADD  CONSTRAINT [FK_orderlines_agegroups] FOREIGN KEY([AGEGROUP_ID])
REFERENCES  [dim].[AGEGROUPS] ([AGEGROUP_ID])
GO
ALTER TABLE [fact].[ORDERLINES] CHECK CONSTRAINT [FK_orderlines_agegroups]
GO
------------------------ ORDERLINE - CUSTOMER
ALTER TABLE [fact].[ORDERLINES]  WITH CHECK ADD  CONSTRAINT [FK_orderlines_customer] FOREIGN KEY([CUSTOMER_ID])
REFERENCES  [dim].[CUSTOMER] ([CUSTOMER_ID])
GO
ALTER TABLE [fact].[ORDERLINES] CHECK CONSTRAINT [FK_orderlines_customer]
GO
------------------------ ORDERLINE - GENDER
ALTER TABLE [fact].[ORDERLINES]  WITH CHECK ADD  CONSTRAINT [FK_orderlines_gender] FOREIGN KEY([GENDER_ID])
REFERENCES  [dim].[GENDER] ([GENDER_ID])
GO
ALTER TABLE [fact].[ORDERLINES] CHECK CONSTRAINT [FK_orderlines_gender]
GO
------------------------ ORDERLINE - GEOGRAPHY
ALTER TABLE [fact].[ORDERLINES]  WITH CHECK ADD  CONSTRAINT [FK_orderlines_geography] FOREIGN KEY([GEOGRAPHY_ID])
REFERENCES  [dim].[GEOGRAPHY] ([GEOGRAPHY_ID])
GO
ALTER TABLE [fact].[ORDERLINES] CHECK CONSTRAINT [FK_orderlines_geography]
GO
------------------------ ORDERLINE - INCOME
ALTER TABLE [fact].[ORDERLINES]  WITH CHECK ADD  CONSTRAINT [FK_orderlines_income] FOREIGN KEY([INCOME_ID])
REFERENCES  [dim].[INCOME] ([INCOME_ID])
GO
ALTER TABLE [fact].[ORDERLINES] CHECK CONSTRAINT [FK_orderlines_income]
GO
