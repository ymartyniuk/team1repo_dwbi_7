USE DW_DS2
GO
----------------------------------------------------------------------------------------
------------------------------- CALENDAR INSERT ----------------------------------------
----------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT TOP 1 * FROM [dim].[CALENDAR])
BEGIN
PRINT 'DATE INSERTED'
DECLARE @MIN_DATE DATE = (SELECT MIN([ORDERDATE]) FROM [DS2].[dbo].[ORDERS]),
        @MAX_DATE DATE = (SELECT MAX([ORDERDATE]) FROM [DS2].[dbo].[ORDERS]),
        @NUMBER_OF_YEARS INT

SET DATEFIRST 7;
SET DATEFORMAT mdy;
SET LANGUAGE US_ENGLISH;

SET @NUMBER_OF_YEARS = (DATEPART(YEAR,@MAX_DATE)-DATEPART(YEAR,@MIN_DATE))+1

DECLARE @CUTOFF_DATE DATE = DATEADD(YEAR, @NUMBER_OF_YEARS, @MIN_DATE);
 
INSERT INTO [dim].[CALENDAR]([DATE_ID]) 
SELECT d
FROM
(
  SELECT d = DATEADD(DAY, rn - 1, @MIN_DATE)
  FROM 
  (
    SELECT TOP (DATEDIFF(DAY, @MIN_DATE, @CUTOFF_DATE)) 
      rn = ROW_NUMBER() OVER (ORDER BY s1.[object_id])
    FROM sys.all_objects AS s1
    CROSS JOIN sys.all_objects AS s2
    -- on my system this would support > 5 million days
    ORDER BY s1.[object_id]
  ) AS x
) AS y
END
GO

----------------------------------------------------------------------------------------
------------------------------- AGEGROPS INSERT ----------------------------------------
----------------------------------------------------------------------------------------

INSERT INTO [dim].[AGEGROUPS] ([AGEGROUP],[MIN_AGE],[MAX_AGE]) 
VALUES ('0 - 18',0,18),('19 - 28',19,28),('29 - 45',29,45),
       ('46 - 60',46,60),('61 - ',61,200)
GO

----------------------------------------------------------------------------------------
------------------------------- GENDER INSERT ----------------------------------------
----------------------------------------------------------------------------------------                                                                     

INSERT INTO [dim].[GENDER] (GENDER)
VALUES ('F'),('M')
GO

----------------------------------------------------------------------------------------
------------------------------- INCOME INSERT ----------------------------------------
----------------------------------------------------------------------------------------    

INSERT INTO [dim].[INCOME]([INCOME],[MIN_INCOME],[MAX_INCOME])
VALUES ('0 - 20000',0,20000),('20001 - 40000',20001,40000),('40001 - 60000',40001,60000),
       ('60001 - 80000',60001,80000),('80001 - 100000',80001,100000)
GO