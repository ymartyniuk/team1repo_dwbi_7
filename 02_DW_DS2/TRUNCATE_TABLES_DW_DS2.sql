USE [DW_DS2]
GO

-------------------------------------------------------------------------------------------------
-----------------------------  DROP FOREIGN KEYS ------------------------------------------------
-------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dim.FK_geo_geography')
  )
ALTER TABLE [dim].[GEOGRAPHY] DROP CONSTRAINT [FK_geo_geography]
GO
-------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------
IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'fact.FK_orderlines_product')
  )
ALTER TABLE [fact].[ORDERLINES] DROP CONSTRAINT [FK_orderlines_product]
GO
-------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'fact.FK_orderlines_calendar')
  )
ALTER TABLE [fact].[ORDERLINES] DROP CONSTRAINT [FK_orderlines_calendar]
GO
-------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'fact.FK_orderlines_agegroups')
  )
ALTER TABLE [fact].[ORDERLINES] DROP CONSTRAINT [FK_orderlines_agegroups]
GO
-------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'fact.FK_orderlines_customer')
  )
ALTER TABLE [fact].[ORDERLINES] DROP CONSTRAINT [FK_orderlines_customer]
GO
-------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'fact.FK_orderlines_gender')
  )
ALTER TABLE [fact].[ORDERLINES] DROP CONSTRAINT [FK_orderlines_gender]
GO
-------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'fact.FK_orderlines_geography')
  )
ALTER TABLE [fact].[ORDERLINES] DROP CONSTRAINT [FK_orderlines_geography]
GO
-------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'fact.FK_orderlines_income')
  )
ALTER TABLE [fact].[ORDERLINES] DROP CONSTRAINT [FK_orderlines_income]
GO
-------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'fact.FK_orderlines_order')
  )
ALTER TABLE [fact].[ORDERLINES] DROP CONSTRAINT [FK_orderlines_order]
GO
-----------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------
IF OBJECT_ID('fact.ORDERS', 'U') IS NOT NULL 
TRUNCATE TABLE [fact].[ORDERS]
GO
IF OBJECT_ID('fact.ORDERLINES', 'U') IS NOT NULL 
TRUNCATE TABLE [fact].[ORDERLINES]
GO
IF OBJECT_ID('dim.AGEGROUPS', 'U') IS NOT NULL 
TRUNCATE TABLE [dim].[AGEGROUPS]
GO
IF OBJECT_ID('dim.CALENDAR', 'U') IS NOT NULL 
TRUNCATE TABLE [dim].[CALENDAR]
GO
IF OBJECT_ID('dim.CUSTOMER', 'U') IS NOT NULL 
TRUNCATE TABLE [dim].[CUSTOMER]
GO
IF OBJECT_ID('dim.GENDER', 'U') IS NOT NULL 
TRUNCATE TABLE [dim].[GENDER]
GO
IF OBJECT_ID('dim.GEOGRAPHY', 'U') IS NOT NULL 
TRUNCATE TABLE [dim].[GEOGRAPHY]
GO
IF OBJECT_ID('dim.INCOME', 'U') IS NOT NULL 
TRUNCATE TABLE [dim].[INCOME]
GO
IF OBJECT_ID('dim.PRODUCT', 'U') IS NOT NULL 
TRUNCATE TABLE [dim].[PRODUCT]
GO
-------------------------------------------------------------------------------------------------
------------------------------ FOREIGN KEYS -----------------------------------------------------
-------------------------------------------------------------------------------------------------
------------------------ GEOGRAPHY - GEOGRAFY
ALTER TABLE [dim].[GEOGRAPHY]  WITH CHECK ADD  CONSTRAINT [FK_geo_geography] FOREIGN KEY([GEO_PARENT_ID])
REFERENCES  [dim].[GEOGRAPHY] ([GEOGRAPHY_ID])
GO
ALTER TABLE [dim].[GEOGRAPHY] CHECK CONSTRAINT [FK_geo_geography]
GO
-------------------------------------------------------------------------------------------------
------------------------ ORDERLINE - PRODUCT
ALTER TABLE [fact].[ORDERLINES]  WITH CHECK ADD  CONSTRAINT [FK_orderlines_product] FOREIGN KEY([PRODUCT_ID])
REFERENCES  [dim].[PRODUCT] ([PRODUCT_ID])
GO
ALTER TABLE [fact].[ORDERLINES] CHECK CONSTRAINT [FK_orderlines_product]
GO
-------------------------------------------------------------------------------------------------
------------------------ ORDERLINE - CALENDAR
ALTER TABLE [fact].[ORDERLINES]  WITH CHECK ADD  CONSTRAINT [FK_orderlines_calendar] FOREIGN KEY([DATE_ID])
REFERENCES  [dim].[CALENDAR] ([DATE_ID])
GO
ALTER TABLE [fact].[ORDERLINES] CHECK CONSTRAINT [FK_orderlines_calendar]
GO
------------------------ ORDERLINE - AGEGROUPS
ALTER TABLE [fact].[ORDERLINES]  WITH CHECK ADD  CONSTRAINT [FK_orderlines_agegroups] FOREIGN KEY([AGEGROUP_ID])
REFERENCES  [dim].[AGEGROUPS] ([AGEGROUP_ID])
GO
ALTER TABLE [fact].[ORDERLINES] CHECK CONSTRAINT [FK_orderlines_agegroups]
GO
------------------------ ORDERLINE - CUSTOMER
ALTER TABLE [fact].[ORDERLINES]  WITH CHECK ADD  CONSTRAINT [FK_orderlines_customer] FOREIGN KEY([CUSTOMER_ID])
REFERENCES  [dim].[CUSTOMER] ([CUSTOMER_ID])
GO
ALTER TABLE [fact].[ORDERLINES] CHECK CONSTRAINT [FK_orderlines_customer]
GO
------------------------ ORDERLINE - GENDER
ALTER TABLE [fact].[ORDERLINES]  WITH CHECK ADD  CONSTRAINT [FK_orderlines_gender] FOREIGN KEY([GENDER_ID])
REFERENCES  [dim].[GENDER] ([GENDER_ID])
GO
ALTER TABLE [fact].[ORDERLINES] CHECK CONSTRAINT [FK_orderlines_gender]
GO
------------------------ ORDERLINE - GEOGRAPHY
ALTER TABLE [fact].[ORDERLINES]  WITH CHECK ADD  CONSTRAINT [FK_orderlines_geography] FOREIGN KEY([GEOGRAPHY_ID])
REFERENCES  [dim].[GEOGRAPHY] ([GEOGRAPHY_ID])
GO
ALTER TABLE [fact].[ORDERLINES] CHECK CONSTRAINT [FK_orderlines_geography]
GO
------------------------ ORDERLINE - INCOME
ALTER TABLE [fact].[ORDERLINES]  WITH CHECK ADD  CONSTRAINT [FK_orderlines_income] FOREIGN KEY([INCOME_ID])
REFERENCES  [dim].[INCOME] ([INCOME_ID])
GO
ALTER TABLE [fact].[ORDERLINES] CHECK CONSTRAINT [FK_orderlines_income]
GO
------------------------ ORDERLINE - ORDER_ID
ALTER TABLE [fact].[ORDERLINES]  WITH CHECK ADD  CONSTRAINT [FK_orderlines_order] FOREIGN KEY([ORDER_ID])
REFERENCES  [fact].[ORDERS] ([ORDER_ID])
GO
ALTER TABLE [fact].[ORDERLINES] CHECK CONSTRAINT [FK_orderlines_order]
GO