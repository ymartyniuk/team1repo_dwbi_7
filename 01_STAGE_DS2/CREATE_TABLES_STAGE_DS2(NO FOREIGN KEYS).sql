USE [STAGE_DS2]
GO

-------------------------------------------------------------------------------------------------
-----------------------------  DROP TABLES -----------------------------------------------------
-------------------------------------------------------------------------------------------------

IF OBJECT_ID('dbo.CATEGORIES', 'U') IS NOT NULL 
DROP TABLE [dbo].[CATEGORIES]

GO

IF OBJECT_ID('dbo.PRODUCTS', 'U') IS NOT NULL 
DROP TABLE [dbo].[PRODUCTS]
GO

IF OBJECT_ID('dbo.CUSTOMERS', 'U') IS NOT NULL 
DROP TABLE [dbo].[CUSTOMERS]
GO

IF OBJECT_ID('dbo.ORDERS', 'U') IS NOT NULL 
DROP TABLE [dbo].[ORDERS]
GO

IF OBJECT_ID('dbo.ORDERLINES', 'U') IS NOT NULL 
DROP TABLE [dbo].[ORDERLINES]
GO
-------------------------------------------------------------------------------------------------
----------------------------- TABLES CREATION ---------------------------------------------------
-------------------------------------------------------------------------------------------------
CREATE TABLE [dbo].[CATEGORIES](
    [CATEGORY] [tinyint] NOT NULL,
    [CATEGORYNAME] [varchar](50) NOT NULL,
	[CREATE_DATE] [DATETIME] NOT NULL,
	[UPDATE_DATE] [DATETIME] NOT NULL
)
GO
-------------------------------------------------------------------------------------------------
CREATE TABLE [dbo].[CUSTOMERS](
	[CUSTOMERID] [int] NOT NULL,
	[FIRSTNAME] [varchar](50) NOT NULL,
	[LASTNAME] [varchar](50) NOT NULL,
	[CITY] [varchar](50) NOT NULL,
	[STATE] [varchar](50) NULL,
	[COUNTRY] [varchar](50) NOT NULL,
	[REGION] [tinyint] NOT NULL,
	[EMAIL] [varchar](50) NULL,
	[PHONE] [varchar](50) NULL,
	[AGE] [tinyint] NULL,
	[INCOME] [int] NULL,
	[GENDER] [varchar](1) NULL,
	[CREATE_DATE] [DATETIME] NOT NULL,
	[UPDATE_DATE] [DATETIME] NOT NULL
)
GO
-------------------------------------------------------------------------------------------------
CREATE TABLE [dbo].[PRODUCTS](
	[PROD_ID] [int] NOT NULL,
	[CATEGORY] [tinyint] NOT NULL,
	[TITLE] [varchar](50) NOT NULL,
	[ACTOR] [varchar](50) NOT NULL,
	[PRICE] [money] NOT NULL,
	[CREATE_DATE] [DATETIME] NOT NULL,
	[UPDATE_DATE] [DATETIME] NOT NULL
 )
GO
-------------------------------------------------------------------------------------------------
CREATE TABLE [dbo].[ORDERS](
	[ORDERID] [int] NOT NULL,
	[ORDERDATE] [datetime] NOT NULL,
	[CUSTOMERID] [int] NOT NULL,
	[NETAMOUNT] [money] NOT NULL,
	[TAX] [money] NOT NULL,
	[TOTALAMOUNT] [money] NOT NULL,
	[CREATE_DATE] [DATETIME] NOT NULL,
	[UPDATE_DATE] [DATETIME] NOT NULL
)

GO
-------------------------------------------------------------------------------------------------
CREATE TABLE [dbo].[ORDERLINES](
	[ORDERLINEID] [smallint] NOT NULL,
	[ORDERID] [int] NOT NULL,
	[PROD_ID] [int] NOT NULL,
	[QUANTITY] [smallint] NOT NULL,
	[ORDERDATE] [datetime] NOT NULL,
	[CREATE_DATE] [DATETIME] NOT NULL,
	[UPDATE_DATE] [DATETIME] NOT NULL
) 
GO
