USE [STAGE_DS2]
GO
-------------------------------------------------------------------------------------------------
-----------------------------  DROP FOREIGN KEYS ------------------------------------------------
-------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * 
FROM sys.foreign_keys 
WHERE object_id = OBJECT_ID(N'FK_prd_category')
AND parent_object_id = OBJECT_ID(N'dbo.PRODUCTS')
)
ALTER TABLE [dbo].[PRODUCTS] DROP CONSTRAINT [FK_prd_category]
GO

IF EXISTS (SELECT * 
FROM sys.foreign_keys 
WHERE object_id = OBJECT_ID(N'FK_ord_customer')
AND parent_object_id = OBJECT_ID(N'dbo.ORDERS')
)
ALTER TABLE [dbo].[ORDERS] DROP CONSTRAINT [FK_ord_customer]
GO

IF EXISTS (SELECT * 
FROM sys.foreign_keys 
WHERE object_id = OBJECT_ID(N'FK_oL_product')
AND parent_object_id = OBJECT_ID(N'dbo.ORDERLINES')
)
ALTER TABLE [dbo].[ORDERLINES] DROP CONSTRAINT [FK_oL_product]
GO

IF EXISTS (SELECT * 
FROM sys.foreign_keys 
WHERE object_id = OBJECT_ID(N'FK_ORDERID')
AND parent_object_id = OBJECT_ID(N'dbo.ORDERLINES')
)
ALTER TABLE [dbo].[ORDERLINES] DROP CONSTRAINT [FK_ORDERID]
GO
-------------------------------------------------------------------------------------------------

IF OBJECT_ID('dbo.CATEGORIES', 'U') IS NOT NULL 
TRUNCATE TABLE [dbo].[CATEGORIES]

GO

IF OBJECT_ID('dbo.PRODUCTS', 'U') IS NOT NULL 
TRUNCATE TABLE [dbo].[PRODUCTS]
GO

IF OBJECT_ID('dbo.CUSTOMERS', 'U') IS NOT NULL 
TRUNCATE TABLE [dbo].[CUSTOMERS]
GO

IF OBJECT_ID('dbo.ORDERS', 'U') IS NOT NULL 
TRUNCATE TABLE [dbo].[ORDERS]
GO

IF OBJECT_ID('dbo.ORDERLINES', 'U') IS NOT NULL 
TRUNCATE TABLE [dbo].[ORDERLINES]
GO
-------------------------------------------------------------------------------------------------
------------------------------------ FOREIGN KEYS -----------------------------------------------
-------------------------------------------------------------------------------------------------
------------ PRODUCTS - CATEGORY
ALTER TABLE [dbo].[PRODUCTS]  WITH CHECK ADD  CONSTRAINT [FK_prd_category] FOREIGN KEY([CATEGORY])
REFERENCES [dbo].[CATEGORIES] ([CATEGORY])
GO
ALTER TABLE [dbo].[PRODUCTS] CHECK CONSTRAINT [FK_prd_category]
GO
-------------------------------------------------------------------------------------------------
------------ ORDERS - CUSTOMERS
ALTER TABLE [dbo].[ORDERS]  WITH CHECK ADD  CONSTRAINT [FK_ord_customer] FOREIGN KEY([CUSTOMERID])
REFERENCES [dbo].[CUSTOMERS] ([CUSTOMERID])
GO
ALTER TABLE [dbo].[ORDERS] CHECK CONSTRAINT [FK_ord_customer]
GO
-------------------------------------------------------------------------------------------------
------------ ORDERLINES - PRODUCTS
ALTER TABLE [dbo].[ORDERLINES]  WITH CHECK ADD  CONSTRAINT [FK_oL_product] FOREIGN KEY([PROD_ID])
REFERENCES [dbo].[PRODUCTS] ([PROD_ID])
GO
ALTER TABLE [dbo].[ORDERLINES] CHECK CONSTRAINT [FK_oL_product]
GO
------------ ORDERLINES - ORDERS
ALTER TABLE [dbo].[ORDERLINES]  WITH CHECK ADD  CONSTRAINT [FK_ORDERID] FOREIGN KEY([ORDERID])
REFERENCES [dbo].[ORDERS] ([ORDERID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ORDERLINES] CHECK CONSTRAINT [FK_ORDERID]
GO
-------------------------------------------------------------------------------------------------